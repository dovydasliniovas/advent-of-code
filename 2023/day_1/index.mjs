// @ts-check

/**
 * Advent of code 2023 Day 1
 * @see https://adventofcode.com/2023/day/1
 */

import path from "path";
import { readFile, writeFile } from "fs/promises";

/**
 * @typedef {Object} CaseOutput
 * @property {number} calibrationTotal
 * @property {Object[]} output
 */

const regex = /\d+/g;

/**
 * @description
 * Digit names map to convert them to numbers

 * @type {Object<string, number>}
*/
const digitNamesMap = {
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
};

/**
 * @description
 * Main function
 *
 * @param {string} inputPath
 * @param {string} outputPath
 * @param {boolean} convertDigitNames
 */
async function runCase(inputPath, outputPath, convertDigitNames) {
  const absoluteInputPath = path.resolve(
    path.dirname(process.argv[1]),
    inputPath
  );

  const absoluteOutputPath = path.resolve(
    path.dirname(process.argv[1]),
    outputPath
  );

  const input = await readFile(absoluteInputPath, "utf8");
  const lines = splitFileContentsIntoLines(input);
  const output = computeCalibrationTotal(lines, convertDigitNames);

  await writeFile(absoluteOutputPath, JSON.stringify(output, null, 2));

  console.group("🎄 Calibration:");
  console.info("🧮 Total:", output.calibrationTotal);
  console.info("📄 Output:", path.resolve(outputPath));
  console.groupEnd();
}

/**
 * @description
 * Converts named digits to `{name}{value}`
 * so the digit regex can find them.
 *
 * @param {string} line
 */
function convertDigitNamesToNumbers(line) {
  return Object.entries(digitNamesMap).reduce(
    /**
     * @description
     * Replacing digit name with `{name}{value}{name}`
     * so cases like `sevenine`, `fiveight` are not made invalid.
     */
    (acc, [key, value]) => acc.replace(key, `${key}${value}${key}`),
    `${line}`.toLowerCase()
  );
}

/**
 * @description
 * Get two-digit number and
 * all the digits from string
 *
 * @param {string} str
 */
function getTwoDigitNumberFromString(str) {
  const match = str.match(regex);

  if (!match) {
    throw new Error(`No digits found in "${str}"`);
  }

  const digits = match.join("");

  return {
    twoDigit: +`${digits.at(0)}${digits.at(-1)}`,
    digits,
  };
}

/**
 * @description
 * Split file contents into lines
 * and remove all `\r` characters
 *
 * @param {string} fileContents
 */
function splitFileContentsIntoLines(fileContents) {
  return fileContents.replace("\r", "").split("\n");
}

/**
 * @description
 * Get the calibration total
 *
 * @param {string[]} contents
 * @param {boolean} convertDigitNames
 */
function computeCalibrationTotal(contents, convertDigitNames) {
  return contents.reduce(
    (acc, line) => {
      /** Skip empty lines */
      if (!line.length) {
        return acc;
      }

      const digitizedLine = convertDigitNames
        ? convertDigitNamesToNumbers(line)
        : line;

      const { digits, twoDigit } = getTwoDigitNumberFromString(digitizedLine);

      if (typeof twoDigit === "number" && !Number.isNaN(twoDigit)) {
        acc.calibrationTotal += twoDigit;
      }

      acc.output.push({
        line,
        digits,
        twoDigit,
        digitizedLine,
      });

      return acc;
    },
    {
      /** @type {number} */ calibrationTotal: 0,
      /** @type {Object[]} */ output: [],
    }
  );
}

/** Cases */
runCase("./input.first.part.test.txt", "./output.first.part.test.json", false);
runCase("./input.first.part.real.txt", "./output.first.part.real.json", false);
runCase("./input.second.part.test.txt", "./output.second.part.test.json", true);
runCase("./input.second.part.real.txt", "./output.second.part.real.json", true);
