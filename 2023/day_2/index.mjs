// @ts-check

/**
 * Advent of code 2023 Day 2
 * @see https://adventofcode.com/2023/day/2
 */

import path from "path";
import { readFile, writeFile } from "fs/promises";

/**
 * Dices configuration definition
 * @typedef {Object} DicesConfiguration
 * @property {number} blue blue dice value
 * @property {number} red red dice value
 * @property {number} green green dice value
 *
 * Game round definition
 * @typedef {DicesConfiguration} Round
 *
 * Game definition
 * @typedef {Object} Game
 * @property {number} id
 * @property {Round[]} rounds
 *
 * Played game definition
 * @typedef {Object} AttemptedGame
 * @property {boolean} isPossible whether the game is possible or not
 * @property {DicesConfiguration} minConfiguration minimum configuration to make the game possible
 *
 * @typedef {Game & AttemptedGame} PlayedGame
 */

const digitRegex = /\d+/g;
const diceRegex = /(\d+) (blue|red|green)/;

/**
 * @description
 * Runs the provided case
 *
 * @param {Object} options
 * @param {string} options.inputPath path to the input file
 * @param {string} options.outputPath path to the output file
 * @param {DicesConfiguration} options.configuration dices configuration
 * @returns {Promise<void>}
 */
async function runCase(options) {
  const { inputPath, outputPath, configuration } = options;

  const dir = path.dirname(process.argv[1]);
  const absoluteInputPath = path.resolve(dir, inputPath);
  const absoluteOutputPath = path.resolve(dir, outputPath);
  const rawGames = await readGameFile(absoluteInputPath);

  const games = parseRawGames(rawGames, configuration);
  const totalGameSum = getSumOfPossibleGameIds(games);
  const totalPowerSum = getSumOfGamePower(games);

  await writeFile(
    absoluteOutputPath,
    JSON.stringify(
      {
        games,
        totalGameSum,
        totalPowerSum,
      },
      null,
      2
    )
  );

  console.group("🎄 Playing elf games:");
  console.log(`🧮 Total sum: ${totalGameSum} (part 1)`);
  console.log(`🧮 Total power: ${totalPowerSum} (part 2)`);
  console.info("📄 Output:", absoluteOutputPath);
  console.groupEnd();
}

/**
 * @description
 * Reads the game file and splits it into rounds
 *
 * @param {string} input
 * @returns {Promise<string[]>}
 */
async function readGameFile(input) {
  const rawGames = await readFile(input, "utf8");
  return splitFileContentsIntoLines(rawGames);
}

/**
 * @description
 * Splits provided string into lines
 *
 * @param {string} input
 * @returns {string[]}
 */
function splitFileContentsIntoLines(input) {
  return input.trim().replace("\r", "").split("\n");
}

/**
 * @description
 * Reads provided string and parses it into `@type Game`
 *
 * @param {string[]} input string to parse
 * @param {DicesConfiguration} configuration
 * @returns {PlayedGame[]}
 */
function parseRawGames(input, configuration) {
  return input.map((game) => parseRawGame(game, configuration));
}

/**
 * @description
 * Parses single game
 *
 * @param {string} input string to parse
 * @param {DicesConfiguration} configuration
 * @returns {PlayedGame}
 */
function parseRawGame(input, configuration) {
  const [rawId, rawRounds] = input.split(":");

  /**
   * @description
   * Removes all non-digit characters from the string
   * just in case the input file contains some extra
   * characters.
   */
  const match = rawId.match(digitRegex)?.at(0);

  if (!match) {
    throw new Error(`Invalid game id: ${rawId}`);
  }

  const id = +match;
  const rounds = parseGameRounds(rawRounds);

  /** Play the game to know if it's possible */
  return playGame({
    game: { id, rounds },
    configuration,
  });
}

/**
 * @description
 * Reads provided string and parses it into game rounds
 *
 * @param {string} input string to parse
 * @returns {Round[]}
 */
function parseGameRounds(input) {
  const rounds = input.split(";");
  return rounds.map(parseGameRound);
}

/**
 * @description
 * Reads provided string and parses it into `@type Round`
 *
 * @param {string} input string to parse
 * @returns {DicesConfiguration}
 */
function parseGameRound(input) {
  const dices = input.split(",");

  return dices.reduce(
    (acc, dice) => {
      const match = dice.match(diceRegex);

      if (!match) {
        return acc;
      }

      const [, rawValue, color] = match;
      acc[color] = +rawValue;
      return acc;
    },
    { blue: 0, red: 0, green: 0 }
  );
}

/**
 * @description
 * Attempt to play the provided game
 * and provide whether it's possible or not
 *
 * @param {Object} options
 * @param {Game} options.game games to play
 * @param {DicesConfiguration} options.configuration dices configuration
 * @returns {PlayedGame}
 */
function playGame({ game, configuration }) {
  let isPossible = true;

  /** Determine the minimum configuration to make the game possible */
  const minConfiguration = game.rounds.reduce(
    (acc, round) => {
      const { blue, red, green } = round;

      if (isRoundImpossible(round, configuration)) {
        isPossible = false;
      }

      if (blue > acc.blue) {
        acc.blue = blue;
      }

      if (red > acc.red) {
        acc.red = red;
      }

      if (green > acc.green) {
        acc.green = green;
      }

      return acc;
    },
    { blue: 0, red: 0, green: 0 }
  );

  return {
    ...game,
    isPossible,
    minConfiguration,
  };
}

/**
 * @description
 * Determine if the provided round is possible.
 * If any of the dices is greater than the configuration
 * then the round is impossible and we invert the result
 *
 * @param {Round} round
 * @param {DicesConfiguration} configuration
 * @returns {boolean}
 */
function isRoundImpossible(round, configuration) {
  return (
    round.blue > configuration.blue ||
    round.red > configuration.red ||
    round.green > configuration.green
  );
}

/**
 * @description
 * Sums up the ids of the games
 *
 * @param {PlayedGame[]} games
 * @returns {number}
 */
function getSumOfPossibleGameIds(games) {
  return games.reduce((acc, game) => {
    if (game.isPossible) {
      acc += game.id;
    }

    return acc;
  }, 0);
}

/**
 * @description
 * Sums up the power of the games
 *
 * @param {PlayedGame[]} games
 * @returns {number}
 */
function getSumOfGamePower(games) {
  return games.reduce((acc, { minConfiguration }) => {
    /**
     * The power of a set of cubes is equal to the numbers of red,
     * green, and blue cubes multiplied together
     */
    const power = Object.values(minConfiguration).reduce(
      (acc, value) => acc * value,
      1
    );

    return acc + power;
  }, 0);
}

/** Cases (awaiting due to ordering) */
await runCase({
  inputPath: "./input.test.txt",
  outputPath: "./output.test.json",
  configuration: { red: 12, blue: 14, green: 13 },
});

await runCase({
  inputPath: "./input.real.txt",
  outputPath: "./output.real.json",
  configuration: { red: 12, blue: 14, green: 13 },
});
