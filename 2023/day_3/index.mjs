// @ts-check

/**
 * Advent of code 2023 Day 3
 * @see https://adventofcode.com/2023/day/3
 */

import path from "path";
import { readFile, writeFile } from "fs/promises";

/**
 * @description
 *
 * @typedef {Object} SchematicsBasePoint
 * @property {number} start
 * @property {number} end
 * @property {number} y
 *
 * @typedef {Object} SchematicsSymbolBasePoint
 * @property {string} symbol
 *
 * @typedef {Object} SchematicsNumericBasePoint
 * @property {number} value
 *
 * @typedef {SchematicsNumericPoint | SchematicsSymbolPoint} SchematicsPoint
 * @typedef {SchematicsBasePoint & SchematicsSymbolBasePoint} SchematicsSymbolPoint
 * @typedef {SchematicsBasePoint & SchematicsNumericBasePoint} SchematicsNumericPoint
 *
 * @typedef {Object} SchematicsPointMap
 * @property {SchematicsSymbolPoint[]} symbols
 * @property {SchematicsNumericPoint[]} numbers
 */

const VALID_GEAR_COUNT = 2; // amount of gears to be considered a valid gear ratio

const digitRegex = /\d+/g;
const specialSymbolsRegex = /[\^$&@%#*+?=()/[\]{}\-|\\]/g;

/**
 * @param {Object} options
 * @param {string} options.input
 * @param {string} options.output
 */
async function runCase(options) {
  const { input, output } = options;

  const dir = path.dirname(process.argv[1]);
  const absoluteInputPath = path.resolve(dir, input);
  const absoluteOutputPath = path.resolve(dir, output);

  const rawSchematics = await readSchematicsFile(absoluteInputPath);
  const parsedSchematics = parseSchematics({ schematic: rawSchematics });

  // Part 1
  const partNumbers = getPartNumbers({ schematics: parsedSchematics });
  const partNumberSum = computeNumberSum(partNumbers);

  // Part 2
  const gearRatios = getGearRatios({ schematics: parsedSchematics });
  const gearRatioSum = computeNumberSum(gearRatios);

  await writeFile(
    absoluteOutputPath,
    JSON.stringify(
      {
        parsedSchematics,
        partNumberSum,
        gearRatioSum,
        partNumbers,
      },
      null,
      2
    )
  );

  console.group("🎄 Reading schematics:");
  console.log(`🧮 Total sum: ${partNumberSum} (part 1)`);
  console.log(`🧮 Total gear sum: ${gearRatioSum} (part 2)`);
  console.info("📄 Output:", absoluteOutputPath);
  console.groupEnd();
}

/**
 * @description
 * Read the schematics file
 *
 * @param {string} path
 */
async function readSchematicsFile(path) {
  const rawSchematics = await readFile(path, "utf-8");
  const schematics = rawSchematics.trim().split("\r\n");
  return schematics;
}

/**
 * @description
 * Parse schematics file
 *
 * @param {Object} options
 * @param {string[]} options.schematic
 */
function parseSchematics({ schematic }) {
  const schematics = schematic.map((line, lineIndex) =>
    parseSchematicsLine({ line, lineIndex })
  );

  const numbers = schematics.map((line) => line.numbers).flat();
  const symbols = schematics.map((line) => line.symbols).flat();

  return { numbers, symbols };
}

/**
 * @description
 * Parses schematics line.
 *
 * @param {Object} options
 * @param {string} options.line
 * @param {number} options.lineIndex
 * @return {SchematicsPointMap}
 */
function parseSchematicsLine({ line, lineIndex }) {
  /** @type {SchematicsPointMap} */
  const points = {
    numbers: [],
    symbols: [],
  };

  const numberIndices = Array.from(line.matchAll(digitRegex));
  const symbolIndices = Array.from(line.matchAll(specialSymbolsRegex));

  if (numberIndices.length) {
    numberIndices.forEach((match) => {
      if (typeof match.index !== "number") {
        return;
      }

      const value = match[0];
      const end = match.index + value.length;

      points.numbers.push({
        start: match.index,
        value: +value,
        y: lineIndex,
        end,
      });
    }, []);
  }

  if (symbolIndices.length) {
    symbolIndices.forEach((match) => {
      if (typeof match.index !== "number") {
        return;
      }

      const symbol = match[0];
      const end = match.index + symbol.length;

      points.symbols.push({
        start: match.index,
        y: lineIndex,
        symbol,
        end,
      });
    }, []);
  }

  return points;
}

/**
 * @description
 * Get part numbers from schematics:
 * "any number adjacent to a symbol, even diagonally, is a part number"
 *
 * @param {Object} options
 * @param {SchematicsPointMap} options.schematics
 * @return {number[]}
 */
function getPartNumbers({ schematics }) {
  const { numbers, symbols } = schematics;

  /**
   * Loop through symbols and check whether
   * there is a number adjacent to it or previous/next line
   */
  const partNumbersSymbols = symbols.map((symbol) => {
    return numbers.filter((number) => {
      const { isDiagonal, isNextOrPreviousCharacter } =
        getNumberMetaDataFromSymbol({ number, symbol });

      return isNextOrPreviousCharacter || isDiagonal;
    });
  });

  /** Flatten the parts to only return the part numbers */
  return partNumbersSymbols.flatMap((numbers) =>
    numbers.map((number) => number.value)
  );
}

/**
 * @description
 * Get gear ratios from schematics
 *
 * @param {Object} options
 * @param {SchematicsPointMap} options.schematics
 * @return {number[]}
 */
function getGearRatios({ schematics }) {
  const { numbers, symbols } = schematics;

  return symbols.reduce(
    /** @param {number[]} ratio */
    (ratio, symbol) => {
      if (symbol.symbol !== "*") {
        return ratio;
      }

      const horizontalGears = getGearCountByAngle({
        symbol,
        partNumbers: numbers,
        angle: "isNextOrPreviousCharacter",
      });

      const nextDiagonalNumbers = getGearCountByAngle({
        symbol,
        partNumbers: numbers,
        angle: "isNextDiagonal",
      });

      const previousDiagonalNumbers = getGearCountByAngle({
        symbol,
        partNumbers: numbers,
        angle: "isPreviousDiagonal",
      });

      /** Compute total gear count that the `*` is responsible for */
      const totalGearCount =
        horizontalGears.length +
        previousDiagonalNumbers.length +
        nextDiagonalNumbers.length;

      /** If the total gear count is valid, compute the gear ratio */
      if (totalGearCount === VALID_GEAR_COUNT) {
        const gearRatio =
          computeGearRatio(previousDiagonalNumbers) *
          computeGearRatio(horizontalGears) *
          computeGearRatio(nextDiagonalNumbers);

        ratio.push(gearRatio);
      }

      return ratio;
    },
    []
  );
}

/**
 * @description
 * Check if provided number meta data
 * against provided symbol meta data
 *
 * @param {Object} options
 * @param {SchematicsNumericPoint} options.number
 * @param {SchematicsSymbolPoint} options.symbol
 */
function getNumberMetaDataFromSymbol({ number, symbol }) {
  const isNextCharacter = number.end === symbol.start && number.y === symbol.y;

  const isPreviousCharacter =
    number.start === symbol.end && number.y === symbol.y;

  const isDiagonalRange =
    number.start <= symbol.end && number.end >= symbol.start;

  const isNextLine = number.y === symbol.y + 1;
  const isNextDiagonal = isNextLine && isDiagonalRange;

  const isPreviousLine = number.y === symbol.y - 1;
  const isPreviousDiagonal = isPreviousLine && isDiagonalRange;

  const isDiagonal = isPreviousDiagonal || isNextDiagonal;
  const isNextOrPreviousCharacter = isNextCharacter || isPreviousCharacter;

  return {
    isNextCharacter,
    isPreviousCharacter,
    isNextOrPreviousCharacter,

    isDiagonal,
    isNextDiagonal,
    isPreviousDiagonal,
  };
}

/**
 * @description
 * Get gear count based on line angle
 *
 * @param {Object} options
 * @param {SchematicsSymbolPoint} options.symbol
 * @param {SchematicsNumericPoint[]} options.partNumbers
 * @param {keyof ReturnType<typeof getNumberMetaDataFromSymbol>} options.angle
 * @return {number[]}
 */
function getGearCountByAngle({ symbol, partNumbers, angle }) {
  return partNumbers.reduce(
    /** @param {number[]} gears */
    (gears, number) => {
      const methods = getNumberMetaDataFromSymbol({ number, symbol });

      if (methods[angle]) {
        gears.push(number.value);
      }

      return gears;
    },
    []
  );
}

/**
 * @description
 * Compute part number sum
 *
 * @param {number[]} numbers
 * @return {number}
 */
function computeNumberSum(numbers) {
  return numbers.reduce((acc, curr) => acc + curr, 0);
}

/**
 * @description
 * Compute gear ratio
 *
 * @param {number[]} numbers
 * @return {number}
 */
function computeGearRatio(numbers) {
  return numbers.reduce((acc, curr) => acc * curr, 1);
}

console.time("(Part 1)");
await runCase({
  input: "./input.test.txt",
  output: "./output.test.json",
});
console.timeEnd("(Part 1)");

console.time("(Part 2)");
await runCase({
  input: "./input.real.txt",
  output: "./output.real.json",
});
console.timeEnd("(Part 2)");
