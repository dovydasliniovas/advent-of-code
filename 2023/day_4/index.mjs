// @ts-check

/**
 * Advent of code 2023 Day 4
 * @see https://adventofcode.com/2023/day/4
 */

import path from "path";
import { readFile } from "fs/promises";

/**
 * @typedef {Object} ScratchCard
 * @property {number} gameId
 * @property {number[]} winningNumbers
 * @property {number[]} playerNumbers
 *
 * @typedef {Object} ScratchCardPoints
 * @property {number} gameId
 * @property {number} points
 * @property {number[]} matchingWinningNumbers
 */

/** Scratch card line `Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53` unknown space length */
const scratchCardRegex =
  /Card[ \t]+(\d+):[ \t]+([\d ]+)[ \t]+\|[ \t]+([\d ]+)/g;

/**
 * @param {Object} options
 * @param {string} options.input
 */
async function runCase(options) {
  const { input } = options;

  const dir = path.dirname(process.argv[1]);
  const absoluteInputPath = path.resolve(dir, input);
  const cards = await readScratchCardsFile(absoluteInputPath);

  /** Compute the points and winning numbers for each scratch card */
  const scratchCardPoints = cards.map(computeScratchCardPoints);

  /** Part 1 */
  const totalWinningPoints = computeTotalPoints(scratchCardPoints);

  /** Part 2 */
  const totalScratchCards = computeTotalScratchCards(scratchCardPoints);

  console.group("🎄 Scratch cards:");
  console.log(`🧮 Total points: ${totalWinningPoints} (part 1)`);
  console.log(`🧮 Total cards: ${totalScratchCards} (part 2)`);
  console.groupEnd();
}

/**
 * @description
 * Read the list of scratch cards from the input file
 *
 * @param {string} inputPath
 * @returns {Promise<ScratchCard[]>}
 */
async function readScratchCardsFile(inputPath) {
  const rawScratchCards = await readFile(inputPath, "utf8");
  return parseScratchCards(rawScratchCards);
}

/**
 * @description
 * Parse the list of scratch cards
 *
 * @param {string} rawScratchCards
 * @returns {ScratchCard[]}
 */
function parseScratchCards(rawScratchCards) {
  const rawCardData = Array.from(rawScratchCards.matchAll(scratchCardRegex));

  return rawCardData.map(([, id, playerNumbers, winningNumbers]) => ({
    gameId: Number(id),
    playerNumbers: playerNumbers.split(" ").map(Number),
    winningNumbers: winningNumbers.split(" ").map(Number),
  }));
}

/**
 * @description
 * Compute player points
 * - first match 1 point
 * - next matching numbers double the points
 * - returns winning number count
 *
 * @param {ScratchCard} ScratchCard
 * @returns {ScratchCardPoints}
 */
function computeScratchCardPoints({ gameId, playerNumbers, winningNumbers }) {
  return playerNumbers.reduce(
    /** @param {ScratchCardPoints} acc */
    (acc, number) => {
      if (number && winningNumbers.includes(number)) {
        acc.matchingWinningNumbers.push(number);
        acc.points = acc.points === 0 ? 1 : acc.points * 2;
      }

      return acc;
    },
    { gameId, points: 0, matchingWinningNumbers: [] }
  );
}

/**
 * @description
 * Compute the total points for all scratch cards
 *
 * @param {ScratchCardPoints[]} scratchCardsPoints
 */
function computeTotalPoints(scratchCardsPoints) {
  return scratchCardsPoints.reduce((acc, { points }) => acc + points, 0);
}

/**
 * @description
 * Play the scratch card:
 * - Loop through each card and check the count of winning numbers
 * -- check the following cards for the same count of winning numbers
 * -- if found, add to the total and recursively check the other card counts
 *
 * @param {ScratchCardPoints[]} scratchCardsPoints
 * @returns {number}
 */
function computeTotalScratchCards(scratchCardsPoints) {
  let scratchCardCount = 0;

  /**
   * @param {ScratchCardPoints[]} cards
   * @param {number} index
   */
  function checkRemainingCards(cards, index) {
    const { matchingWinningNumbers } = cards[index];
    const winningCardLength = matchingWinningNumbers.length;

    scratchCardCount += 1;

    if (winningCardLength > 0) {
      const nextCardBatchIndex = index + 1;
      const endOfNextBatchCardIndex = nextCardBatchIndex + winningCardLength;

      // Check the other cards for the same count of winning numbers
      const nextScratchCardBatch = cards.slice(
        nextCardBatchIndex,
        endOfNextBatchCardIndex
      );

      nextScratchCardBatch.forEach((_, i) => {
        checkRemainingCards(cards, nextCardBatchIndex + i);
      });
    }
  }

  scratchCardsPoints.forEach((_, index) => {
    checkRemainingCards(scratchCardsPoints, index);
  });

  return scratchCardCount;
}

console.time("Part 1");
await runCase({ input: "input.test.txt" });
console.timeEnd("Part 1");

console.time("Part 2");
await runCase({ input: "input.real.txt" });
console.timeEnd("Part 2");
