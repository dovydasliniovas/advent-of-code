# Advent of code

For the `Advent of code` challenges.

- [2023 / Advent of code](./2023/)
  - [Day 1 - Trebuchet?!](./2023/day_1/)
  - [Day 2 - Cube Conundrum](./2023/day_2/)
  - [Day 3 - Gear Ratios](./2023/day_3/)
  - [Day 4 - Scratchcards](./2023/day_4/)

---

Find at [adventofcode.com](https://adventofcode.com/)
